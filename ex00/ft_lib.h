/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lib.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 12:10:57 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:08:34 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIB_H
# define FT_LIB_H
# define INT_MAX +2147483647
# define INT_MIN -2147483648
# ifndef NULL
#  define NULL (void *)0
# endif
# include "ft_string.h"
# include "ft_io.h"

int			ft_pow(int base, int power);
int			ft_atoi(char *str);
int			ft_atoi_ptr(char *str, int *len);
int			ft_number_roof(int val, int roof);

char		*ft_itoa_base(int val, int base);
char		*ft_itoa(int value);

#endif
