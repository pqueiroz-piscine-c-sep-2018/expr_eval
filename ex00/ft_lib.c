/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lib.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 11:18:21 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/06 22:08:15 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"

int			ft_number_roof(int val, int roof)
{
	return ((val <= roof) ? val : roof);
}

static int	ft_valid_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] >= '0' && str[i] <= '9')
		i++;
	return (i);
}

int			ft_pow(int base, int power)
{
	int res;

	if (power == 0)
		return (1);
	if (power == 1)
		return (base);
	res = base;
	while (--power > 0)
		res *= base;
	return (res);
}

int			ft_atoi_ptr(char *str, int *len)
{
	int i;
	int res;
	int mult;

	while (IS_WHITESPACE(*str))
		str++;
	res = 0;
	mult = (*str == '-' && (str++ || 1)) ? -1 : 1;
	mult = ((*str == '+' && mult == 1 && (str++ || 1)) || mult == 1) ? 1 : -1;
	i = ft_valid_strlen(str);
	if (len)
		*len = i + ((mult == -1) ? 1 : 0);
	while (*str >= '0' && *str <= '9')
	{
		if (res > INT_MAX / 10 || (res == INT_MAX / 10 && *str - '0' > 7))
		{
			if (len)
				*len = (mult == 1) ? 10 : 11;
			return ((mult == 1) ? INT_MAX : INT_MIN);
		}
		res += (*str - '0') * ft_pow(10, i - 1);
		str++;
		i--;
	}
	return (res * mult);
}

int			ft_atoi(char *str)
{
	return (ft_atoi_ptr(str, NULL));
}
