/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 11:09:49 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/06 22:17:54 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRING_H
# define FT_STRING_H
# define IS_WHITESPACE(c) (c == ' ' || c == '\t' || c == '\n')
# include "ft_lib.h"
# include "ft_io.h"
# include <stdlib.h>
# include <stdio.h>

char		*ft_substr_s(char *src, int n, int start);
char		*ft_substr(char *src, int n);
char		*ft_str_replace(char *src, char *find, char *repl);
char		*ft_str_replace_n(char *src, char *find, char *repl, int times);
char		*ft_strcat(char *s1, char *s2);

int			ft_str_indexof(char *str, char find);
int			ft_strlen(char *str);
int			ft_strcmp(char *s1, char *s2);

#endif
