/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 12:32:02 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:19:34 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_eval.h"
#include <stdio.h>
#define IS_NUM(x) (x <= '9' && x >= '0')

int		expr_left_index(char *str, int start)
{
	int len;

	len = ft_strlen(str);
	while (start > 0 && start < len)
	{
		start--;
		if (!IS_NUM(str[start]))
		{
			if (!IS_NUM(str[start - 1]))
				return (start);
			else
				return (start + 1);
		}
	}
	return (start);
}

int		expr_get_mathing(char *str, int start, char c, int d)
{
	int		i;
	int		op;
	int		len;
	char	m;

	m = (c == '(') ? ')' : '(';
	op = 0;
	i = start + d;
	len = ft_strlen(str);
	while (i > 0 && i < len)
	{
		op = (str[i] == c) ? op + 1 : op;
		if (str[i] == m)
		{
			if (op == 0)
				return (i);
			else
				op--;
		}
		i += d;
	}
	return (-1);
}

char	*ft_call_opr(char *str, int index)
{
	char	opr[2];
	int		b;
	int		a;
	int		aux;
	int		finish;

	opr[0] = str[index];
	opr[1] = 0;
	b = ft_atoi_ptr(str + index + 1, &aux);
	finish = index + aux;
	index = expr_left_index(str, index);
	a = ft_atoi(str + index);
	str = ft_str_replace_n(str, ft_substr_s(str, finish - index + 1, index),
		ft_itoa(ft_do_opr(a, opr[0], b)), 1);
	return (str);
}

char	*ft_eval(char *str)
{
	int		g;
	int		i;
	char	*aux;

	i = 0;
	if ((i = expr_has_prior(str, 0)) != -1)
	{
		g = expr_get_mathing(str, i, str[i], 1);
		aux = ft_substr_s(str, g - i + 1, i);
		return (ft_eval(ft_str_replace_n(
			str, aux, ft_eval(ft_substr_s(aux, -1, 1)), 1)));
	}
	else if ((i = expr_has_prior(str, 1)) != -1)
	{
		return (ft_eval(ft_call_opr(str, i)));
	}
	else if ((i = expr_has_prior(str, 2)) != -1)
	{
		return (ft_eval(ft_call_opr(str, i)));
	}
	return (str);
}
